/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.Scanner;

/**
 *
 * @author David Shustin
 */
public class Calculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        boolean moreCalc = true;
        double pair[] = new double[2];
        do {
            for (int i = 0; i < 2; i++) {
                boolean validNum = false;
                do {
                    System.out.print("Please enter value #" + (i+1) + ": ");
                    if (sc.hasNext()) {
                        if (sc.hasNextDouble()) {
                            pair[i] = sc.nextDouble();
                            validNum = true;
                        } else {
                            System.err.println(sc.next() + " is not valid input. Please enter valid input.");
                            validNum = false;
                        }
                    }
                } while (!validNum);
            }
            
            boolean validOp = false;
            do {
                System.out.print("Enter an operation: ");
                if (sc.hasNext()) {
                    String answer = sc.next().toLowerCase();
                    switch (answer) {
                        case "multiply":
                            System.out.println(pair[0] * pair[1]);
                            validOp = true;
                            break;
                        case "divide":
                            System.out.println(pair[0] / pair[1]);
                            validOp = true;
                            break;
                        case "add":
                            System.out.println(pair[0] + pair[1]);
                            validOp = true;
                            break;
                        case "subtract":
                            System.out.println(pair[0] - pair[1]);
                            validOp = true;
                            break;
                        default:
                            System.err.println(answer + " is not a valid operation. Please enter a valid operation.");
                            validOp = false;
                            break;
                    }
                }
            } while (!validOp);
            boolean validAns = false;
            do {
                System.out.print("Do you need to do any more calculations? ");
                if (sc.hasNext()) {
                    switch (sc.next().toLowerCase()) {
                        case "yes":
                            validAns = true;
                            moreCalc = true;
                            break;
                        case "no":
                            validAns = true;
                            moreCalc = false;
                            break;
                        default:
                            System.err.println("Please enter yes or no.");
                            validAns = false;
                            break;
                    }
                }
            } while (!validAns);
            
        } while (moreCalc);
        System.out.println("Good-bye!");
    }
    
}
