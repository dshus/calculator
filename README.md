#Calculator
A text-based calculator, written in Java.

#Safety
It built to be safe in all regards.  It will detect non-numbers entered and non-binary answers to yes/no questions, and ask again.

It is also safe when not given one of the operator words (not case sensitive).

**Just try to crash it. (NaN does NOT COUNT AS CRASHING!)
*I dare you.*
**

~~It is NOT, however, safe in regards to min/max numbers. I haven't tested what happens if I go over the maximum for doubles.~~
Just tested. It seems to convert large numbers to scientific notation. Good.

#Usage
Enter two numbers (decimals are allowed!)

Operator words:

* Multiply

* Divide

* Add

* Subtract
